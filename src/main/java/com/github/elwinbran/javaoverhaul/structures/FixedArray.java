/*
 * Copyright (c) 2017 Elwin Slokker
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.github.elwinbran.javaoverhaul.structures;

import com.github.elwinbran.javaoverhaul.base.NullableWrapper;
import java.util.Iterator;

/**
 * A simple array structure that has a fixed amount of places for elements.
 * 
 * @author Elwin Slokker
 * @param <ElementT> The type of element to be stored in the array.
 */
public class FixedArray<ElementT> implements GenericArray<ElementT>
{
    /**
     * 
     */
    private int elements = 0;
    
    /**
     * 
     */
    private final ElementT[] elementData;
    
    /**
     * 
     * @param size The amount of elements that can fit in this array.
     */
    public FixedArray(int size)
    {
        this.elementData = (ElementT[]) new Object[size];
    }

    @Override
    public NullableWrapper<ElementT> set(int index,
                                         NullableWrapper<ElementT> element)
    {
        ElementT old = this.elementData[index];
        NullableWrapper<ElementT> oldElement = null;//TODO init
        if(element.isNil())
        {
            this.elementData[index] = null;
        }
        else
        {
            this.elementData[index] = element.getWrapped();
        }
        return oldElement;
    }

    @Override
    public NullableWrapper<ElementT> get(int index)
    {
        if(this.elementData[index] == null)
        {
            return null;//TODO empty wrapper
        }
        return null;//TODO return wrapper
    }

    @Override
    public void clear()
    {
        for(int i = 0; i < this.elements; i++)
        {
            this.elementData[i] = null;
        }
        this.elements = 0;
    }

    @Override
    public int elementCount()
    {
        return this.elements;
    }

    @Override
    public Iterator<NullableWrapper<ElementT>> getFullContent()
    {
        throw new UnsupportedOperationException("This is a null-implementation.");
    }

    @Override
    public int arrayLength()
    {
        return this.elementData.length;
    }
    
}

