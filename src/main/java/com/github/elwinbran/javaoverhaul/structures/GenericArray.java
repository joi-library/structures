/*
 * Copyright (c) 2017 Elwin Slokker
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.github.elwinbran.javaoverhaul.structures;

import com.github.elwinbran.javaoverhaul.base.NullableWrapper;
import com.github.elwinbran.javaoverhaul.numbers.PositiveNumber;
import java.util.Iterator;

/**
 * Represents a read only array.
 * An array is a data structure that stores data pieces by indices.
 * A single index can store an object and retrieving from an array can only be 
 * done by index.
 * There are no futher restrictions on an array.
 * 
 * NOTE: Zero-based indexing is mandatory.
 * 
 * @author Elwin Slokker
 * @param <ElementT> The type of objects stored in the array.
 */
public interface GenericArray<ElementT>
{
    public abstract NullableWrapper<ElementT> get(PositiveNumber index);
    
    public abstract NullableWrapper<ElementT> set(PositiveNumber index, 
            NullableWrapper<ElementT> element);
    
    public Iterator<NullableWrapper<ElementT>> getFullContent();
    
    public PositiveNumber elementCount();
    
    public PositiveNumber arrayLength();
    
    public void clear();
}
